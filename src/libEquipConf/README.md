# Equipment configuration
This library provides support for dynamic usage of hardware of the same type without the need of recompiling the program when a different model of the same hardware is used.

## Configuration

The hardware configuration is stored in JSON file.
An example of input JSON file is provided in the libEquipConf folder.
Only the following libraries support configuration via json file:
* libPS

The main top-fields of the JSON configuration are:
- version: to ensure compatibility of configuration file with labRemote software version;
- options: version-specific general options (e.g. autoconfiguration of some hardware parameters, if supported);
- devices: list of the hardware devices that are available, and their communication protocol
- channels: list of logical channels available


### Devices section
List of devices that are available, for which the configuration set name, model and communication protocol
An example of hardware configuration follows just for illustration.
Each device must have a unique name and can have the following properties (required ones marked with *)
  - *hw-type: type of hardware device; the supported ones are:
     - "PS": power-supply
  - *hw-model: model for the particular hardware (name of the dedicated class in labPS)
  - *communication: details of communication, can be hardware-specific but generally can include:
     - protocol: communication protocol name (for example, "usb")
     - port: communication port/address 
Custom fields that depend on specific hardware can then be present and used as needed (for example, gpib_addr when using Prologix GIPB to usb connector). Check the .cpp file associated with your power-supply

### Channels section 
List of logical channels. Assigns names to easily map physical channels to logical ones.
Each channel must have a unique name, and can have the following properties (required ones marked witth *)
  - *hw-type: type of hardware device; the supported ones are:
     - "PS": power-supply
  - *device: name of the physical device the channel refers to, as lised in the **devices** section
  - *channel: channel number in the physics device **device**
  - program:  optional list of voltage/current values and voltage/current limis used to program the channel. They are set only if the program() function is called. See description of program function in PowerSupplyChannel class

 See ./src/examples/control_powersupply_example.cpp for an examples on how to control a power supply.

