#
# Prepare the library
add_library(WaveFormGen SHARED)
target_sources(WaveFormGen
  PRIVATE
  PG8133A.cpp
  WF33120A.cpp
  )
target_link_libraries(WaveFormGen PRIVATE Com Utils)
target_include_directories(Meter PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
