#ifndef SERIALCOM_H
#define SERIALCOM_H

#include <string>
#include <termios.h>      

/** 
 * Implementation of block serial communication with POSIX
 * read/write calls.
 */
class SerialCom
{
public:
  /** Available character sizes */
  enum CharSize {CharSize5=CS5,CharSize6=CS6,CharSize7=CS7,CharSize8=CS8};
  
public:
  /**
   * @param deviceName Device corresponding to the serial port
   * @param baud Baud rate to use
   * @param useParityBit Use parity bit
   * @param twoStopBits Use two stop bits instead of one
   * @param flowControl Enable hardware flow control
   * @param charsize Size of a character
   */
  SerialCom(const std::string& deviceName, speed_t baud=B19200, bool parityBit=false, bool twoStopBits=false, bool flowControl=false, CharSize charsize=CharSize::CharSize8);

  ~SerialCom();

  /** Send data to device
   *
   * Throw `std::runtime_error` on error.
   *
   * @param buf Data to be sent
   * @param length Number of characters in `buf` that should be sent
   *
   * @return Number of characters actually sent.
   */  
  int write(char *buf, size_t length);

  /** Read data from device
   *
   * Throw `std::runtime_error` on error.
   *
   * @param buf Buffer where to store results 
   * @param length Number of characters to read.
   *
   * @return Number of characters received
   */
  int read(char *buf, size_t length);

  /** Send data to device  
   *
   * Throw `std::runtime_error` on error.
   *
   * @param buf Data to be sent
   *
   * @return Number of characters actually sent.
   */
  int write(const std::string& buf);

  /** Read at most `MAX_READ` characters from device
   *
   * Throw `std::runtime_error` on error.
   *
   * @param buf Buffer where to store results.
   *
   * @return Number of characters received
   */  
  int read(std::string &buf);

  /** Flush any data in the device buffers
   *
   * @return sett documentation for tcflush
   */
  int flush();

  /** Check if the object is correctly initialized
   *
   * @return true if initialization succeeded, false otherwise
   */
  bool is_open();

private:
  /** Open device and configure via `config()`
   *
   * Also set m_good on sucessful completion
   */
  void init();

  /** Configure device for serial communication
   */
  void config();

  /** Configuration @{ */  

  /// Name of device
  std::string m_deviceName;

  /// Baud rate to use
  speed_t m_baudrate = B19200;

  /// Use parity bit
  bool m_parityBit=false;

  // Use two stop bits intead of one
  bool m_twoStopBits=false;

  // Enable hardware flow control
  bool m_flowControl=false;

  // Size of a character
  CharSize m_charsize=CharSize::CharSize8;

  /** @} */

  /** Run-time variables @{ */    

  /// File handle for communication
  int m_dev =0;

  /// Current settings
  struct termios m_tty;

  /// Settings frm before config
  struct termios m_tty_old;

  /// Good if the `m_dev` is opened and configured correctly
  bool m_good =false;

  /** @} */

  /// Maximum number of characters to read using string buffers
  static const unsigned MAX_READ = 4096;

  /// Temporary buffer for read(std::string&)
  char m_tmpbuf[MAX_READ];
};

#endif
