#include "Logger.h"

loglevel_e incrDebug(loglevel_e loglevel)
{
  switch(loglevel)
    {
    case logDEBUG:
      return logDEBUG1;
    case logDEBUG1:
      return logDEBUG2;
    case logDEBUG2:
      return logDEBUG3;
    case logDEBUG3:
      return logDEBUG4;
    case logDEBUG4:
      return loglevel;
    default:
      return logDEBUG;
    }

  return logDEBUG;
}
