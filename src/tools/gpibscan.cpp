#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "Logger.h"
#include "SerialCom.h"


//------ SETTINGS
loglevel_e loglevel = logINFO;
std::string port;
//---------------


void usage(char *argv[])
{
  std::cerr << "Usage: " << argv[0] << " [options] usbport" << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "List of options:" << std::endl;
  std::cerr << " -d, --debug       Enable more verbose printout"  << std::endl;
  std::cerr << "" << std::endl;
  std::cerr << "" << std::endl;
}

int main(int argc, char*argv[])
{

  if (argc < 1)
    {
      usage(argv);
      return 1;
    }

  //Parse command-line
  int c;
  while (1) {
    int option_index = 0;
    static struct option long_options[] = {
      {"debug",    no_argument      , 0,  'd' },
      {0,          0,                 0,  0 }
    };
    
    c = getopt_long(argc, argv, "d", long_options, &option_index);
    if (c == -1)
      break;

    switch (c)
      {
      case 'd':
	loglevel = logDEBUG;
	break;
      default:
	std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
	std::cerr << std::endl;
	usage(argv);
	return 1;
      }
  }

  if(argc!=optind+1)
    {
      std::cerr << "Required usbport argument missing." << std::endl;
      std::cerr << std::endl;
      usage(argv);
      return 1;
    }
  port=argv[optind++];

  //
  // Create usb connection and scan address ranges

  // Create and initialize connection
  std::shared_ptr<SerialCom> com(new SerialCom(port));
  com->write("++auto 0\n\r");

  // Start scanning
  std::string buf;
  for(uint32_t addr=0;addr<31;addr++)
    {
      std::cout << std::setw(2) << addr << ": ";

      com->write("++addr " + std::to_string(addr) + "\n\r");
      com->write("*IDN?\n\r");
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      com->write("++read eoi\n\r");
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      try
	{
	  buf="";
	  com->read(buf);
	  buf.erase(std::remove(buf.begin(),buf.end(),'\n'),buf.end());
	  buf.erase(std::remove(buf.begin(),buf.end(),'\r'),buf.end());
	  std::cout << buf;
	}
      catch(const std::runtime_error& e)
	{ /* Nothing connected here */ }

      std::cout << std::endl;
    }


  return 0;
}
