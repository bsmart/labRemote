#include "DAC5571.h"

#include "LinearCalibration.h"
#include "NotSupportedException.h"

DAC5571::DAC5571(float reference, std::shared_ptr<I2CCom> com)
  : DACDevice(std::make_shared<LinearCalibration>(reference, 0xFF)), m_com(com)
{ }

DAC5571::~DAC5571()
{ }

void DAC5571::setCount(int32_t counts)
{
  uint16_t command=((counts&0xFF)<<4);
  m_com->write_reg16(command);
}

void DAC5571::setCount(uint8_t ch, int32_t counts)
{
  throw NotSupportedException("DAC5571 does not support multiple channels.");
}

void DAC5571::setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& counts)
{
  throw NotSupportedException("DAC5571 does not support multiple channels.");
}

int32_t DAC5571::readCount()
{
  throw NotSupportedException("DAC5571 does not support reading.");
  return 0;
}
 
int32_t DAC5571::readCount(uint8_t ch)
{
  throw NotSupportedException("DAC5571 does not support reading.");
  return 0;
}
 
void DAC5571::readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data)
{
  data.clear();
  throw NotSupportedException("DAC5571 does not support reading.");
}

