#ifndef DAC5571_H
#define DAC5571_H

#include <memory>

#include "I2CCom.h"
#include "DACDevice.h"

class DAC5571 : public DACDevice
{
public:
  DAC5571(float reference, std::shared_ptr<I2CCom> com);
  virtual ~DAC5571();

  virtual void setCount(int32_t counts);
  virtual void setCount(uint8_t ch, int32_t counts);
  virtual void setCount(const std::vector<uint8_t>& chs, const std::vector<int32_t>& counts);

  virtual int32_t readCount();
  virtual int32_t readCount(uint8_t ch);
  virtual void readCount(const std::vector<uint8_t>& chs, std::vector<int32_t>& data);

private:
  std::shared_ptr<I2CCom> m_com;
};

#endif // DAC5571_H
