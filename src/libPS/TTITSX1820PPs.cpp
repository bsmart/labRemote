#include "TTITSX1820PPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(TTITSX1820PPs)

TTITSX1820PPs::TTITSX1820PPs(const std::string& name) :
IPowerSupply(name)
{ }

void TTITSX1820PPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result=this->receive("*IDN?");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void TTITSX1820PPs::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
  m_com->write("++auto 0\n\r");
}

void TTITSX1820PPs::checkCompatibilityList()
{

  // Check if the PS belong to the list of the support PS models
  std::string idn=this->receive("*IDN?");

  //Add here other version of support Keithley 24xx power supplies
  if(idn.find("THURLBY-THANDAR,TSX1820P,0,1.20") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void TTITSX1820PPs::reset()
{
  send("OPALL 0");
  send("*RST");

  std::string result=receive("*IDN?");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void TTITSX1820PPs::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send("OP 1");
}

void TTITSX1820PPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send("OP 0");
}

void TTITSX1820PPs::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");  
  send("I "+std::to_string(cur));
}

double TTITSX1820PPs::getCurrentLevel(unsigned channel)
{

  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  return std::stod(receive("I?").substr(0,5));
}

void TTITSX1820PPs::setCurrentProtect(double maxcur, unsigned channel)
{
  setCurrentLevel(maxcur, channel);
}

double TTITSX1820PPs::getCurrentProtect(unsigned channel)
{
  return getCurrentLevel(channel);

}

double TTITSX1820PPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(receive("IO?").substr(0,5));
}

void TTITSX1820PPs::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  send("V "+std::to_string(volt));
}

double TTITSX1820PPs::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(receive("V?").substr(0,5));

}

void TTITSX1820PPs::setVoltageProtect(double maxvolt, unsigned channel)
{
  setVoltageLevel(maxvolt, channel);
}

double TTITSX1820PPs::getVoltageProtect(unsigned channel)
{
  return getVoltageLevel(channel);
}

double TTITSX1820PPs::measureVoltage(unsigned channel)
{
  return std::stod(receive("VO?").substr(0,5));
}
void TTITSX1820PPs::send(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string TTITSX1820PPs::receive(const std::string& cmd)
{
  m_com->write("++addr " + std::to_string(m_config["communication"]["gpib_addr"].get<uint32_t>()) + "\n\r");
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\n\r");
  m_com->write("++read eoi\n\r");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}
