#ifndef BK16XXPS_H
#define BK16XXPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/** \brief BK 16xx
 *
 * Implementation for the BK 16xx power supply.
 *
 * [Progamming Manual](https://bkpmedia.s3.amazonaws.com/downloads/programming_manuals/en-us/168xB_programming_manual.pdf)
 */
class Bk16XXPs : public IPowerSupply
{
public:
  Bk16XXPs(const std::string& name);
  ~Bk16XXPs() =default;

  /** \name Communication
   * @{
   */

  virtual void connect();

  virtual void checkCompatibilityList();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();
  virtual void turnOn(unsigned channel);
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

public:
  void connect_usb(const std::string& dev, const speed_t& baud);

private:
  std::unique_ptr<SerialCom> m_com=nullptr;

  void send(const std::string& cmd);
  std::string receive(const std::string& cmd);

  std::chrono::milliseconds m_wait{200};
};

#endif
