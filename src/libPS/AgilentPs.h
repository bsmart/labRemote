#ifndef AGILENTPS_H
#define AGILENTPS_H

#include <chrono>
#include <memory>
#include <string>

#include "IPowerSupply.h"

#include "SerialCom.h"

/**
 * Base implemetation for Agilent/Keysight power supplies that share
 * a very similar command set.
 *
 * Options are possible to add additional for error checking on 
 *  - model compatibility (list of strings partially matched against *idn?, empty means no check is performed)
 *  - channel checking (maximum number of channels, 0 means no check is performed)
 */
class AgilentPs : public IPowerSupply
{
public:
  /**
   * @param name Name of the power supply
   * @param maxChannels Maximum number of channels in the power supply (0 means no maximum)
   * @param models List of supported models (empty means no check is performed)
   */
  AgilentPs(const std::string& name, unsigned maxChannels=0, const std::vector<std::string>& models={});
  ~AgilentPs() =default;

  /** \name Communication
   * @{
   */

  virtual void connect();

  /**
   * Check against supported models.
   *
   * A list of strings (models) is checked against the result of `*IDN?`. If any
   * of them is contained within `*IDN?`, then no error is thrown.
   */
  virtual void checkCompatibilityList();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  virtual void reset();

  /** Turn on all channels
   *
   * Many Agilent power supplies don't have channel-specific enable
   * control.
   *
   * @param channel ignored
   */
  virtual void turnOn(unsigned channel);

  /** Turn off all channels
   *
   * Many Agilent power supplies don't have channel-specific enable
   * control.
   *
   * @param channel ignored
   */
  virtual void turnOff(unsigned channel);

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  virtual void   setCurrentLevel(double cur, unsigned channel = 0);
  virtual double getCurrentLevel(unsigned channel = 0);
  virtual void   setCurrentProtect(double maxcur , unsigned channel = 0);
  virtual double getCurrentProtect(unsigned channel = 0);  
  virtual double measureCurrent(unsigned channel = 0);

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */

  virtual void   setVoltageLevel(double volt, unsigned channel = 0);
  virtual double getVoltageLevel(unsigned channel = 0);
  virtual void   setVoltageProtect(double maxvolt , unsigned channel = 0 );
  virtual double getVoltageProtect(unsigned channel = 0);
  virtual double measureVoltage(unsigned channel = 0);

  /** @} */

  /** \name Model-specific functionality
   * @{
   */

  /** Disable the audible beep
   */
  void beepOff();

  /** @} */

public:
  void connect_usb(const std::string& dev);

protected:
  void send(const std::string& cmd);
  std::string receive(const std::string& cmd);
  
private:
  /** Communication @{ */

  //! Communication device
  std::unique_ptr<SerialCom> m_com=nullptr;

  //! Wait time after each command
  std::chrono::milliseconds m_wait{200};

  /** @} */

  /** PS limitations @{ */

  //! Maximum number of channels, 0 means unlimited
  uint32_t m_maxChannels =0;

  //! List of valid models, empy means no check is performed
  std::vector<std::string> m_models ={};

  /** Set channel for following commands
   *
   * In-range check is performed.
   */
  void setChannel(unsigned channel);

  /** @} */
};

#endif

