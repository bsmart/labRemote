#include "IPowerSupply.h"

#include <iostream>
#include <stdexcept>
#include "Logger.h"

IPowerSupply::IPowerSupply(const std::string& name)
{
  m_name = name;
}

void IPowerSupply::setConfiguration(const nlohmann::json& config)
{  
  m_config = config;
}

const nlohmann::json& IPowerSupply::getConfiguration() const
{
  return m_config;
}

void IPowerSupply::connect(const nlohmann::json& config)
{  
  setConfiguration(config); 
  return connect();
}

void IPowerSupply::checkCompatibilityList()
{
  logger(logWARNING) << "checkCompatibiltyList() not implemented for this PS.";
}

void IPowerSupply::setCurrentProtect(double cur, unsigned channel)
{
  logger(logWARNING) << "setCurrentProtect() not implemented for this PS.";
}

double IPowerSupply::getCurrentProtect(unsigned channel)
{
  logger(logWARNING) << "getCurrentProtect() not implemented for this PS.";
  return 0;
}

void IPowerSupply::setVoltageProtect(double volt, unsigned channel)
{
  logger(logWARNING) << "setVoltageProtect() not implemented for this PS.";
}

double IPowerSupply::getVoltageProtect(unsigned channel)
{
  logger(logWARNING) << "getVoltageProtect() not implemented for this PS.";
  return 0;
}
