#ifndef IPOWERSUPPLY_H
#define IPOWERSUPPLY_H

#include <string>
#include <nlohmann/json.hpp>

/** Generic power supply interface.

    Functions that should be optional but expected to be generic enough
    to end up in this class should not be virtual pure and instead should
    have a dummy implementation in IPowerSupply.cpp throwing an error that such
    functionality is not implemented.

    The hardware configuration is given via a json object and can be harware
    specific. 
    A typical workflow would be:
      IPowerSupply *ps;
      ps->setConfiguration(specificPSConfigJSON);
      ps->connect();
      ps->checkCompatibilityList();
      ps->turnOn();
      ps->setVoltage(1.2, 1); //to change the voltage      
      //....
      ps->turnOff();

    More commonly the power supply will be retrieved from the equipment registry, 
    which will take care of its configuration and connection already, e.g.:
      EquipConf hw(completeConfigJSON);
      td::shared_ptr<IPowerSupply> PS = hw.getPowerSupply("PS");
      ps->turnOn();       
      ps->setVoltage(1.2, 1); //to change the voltage
      //....
      ps->turnOff();
 */
class IPowerSupply
{
public:
  
  /** Constructor. 
   * Note: do NOT do any active command in the constructor. Only initialize local variables.
   */
  IPowerSupply(const std::string& name);

  /** Store JSON hardware configuration 
   * @param config JSON configuration for the given power supply only
   */
  virtual void setConfiguration(const nlohmann::json& config);

  /** Retrieve hardware configuration 
      @return hardware JSON configuration
  */
  const nlohmann::json& getConfiguration() const;

  /** \name Communication
   * @{
   */

  /** \brief Set configuration and Connect to device
   * @param configuration stored as JSON. 
   */
  virtual void connect(const nlohmann::json& config);

  /** \brief Connect to device using existing configuration. */
  virtual void connect() = 0;

  /** \brief Check that the device model is supported.
   *
   * Throw an exception if an unknown power supply model is detected.
   */
  virtual void checkCompatibilityList();

  /** @} */

  /** \name Power Supply Control
   * @{
   */

  /** \brief Execute reset of the device  */
  virtual void reset() = 0 ;  
  
  /** \brief Turn on power supply. 
   * @param channel channel, if any
   */
  virtual void turnOn(unsigned channel = 0) = 0;

  /** \brief Turn off power supply
   * @param channel channel, if any
   */
  virtual void turnOff(unsigned channel = 0) = 0;

  /** @} */
  
  /** \name Current Control and Measurement
   * @{
   */

  /** \brief Set current of PS
   * @param cur current [A]
   * @param channel channel (if any)
   */
  virtual void setCurrentLevel(double cur, unsigned channel = 0) = 0;

  /** \brief Get set current of PS
   * @param channel channel (if any)
   * @return current, convert to [A]
   */
  virtual double getCurrentLevel(unsigned channel = 0) = 0;  

  /** \brief Set current protection [optional]
   * @param cur maximum current [A]
   * @param channel channel (if any)
   */
  virtual void setCurrentProtect(double cur, unsigned channel = 0);

  /** \brief Get current protection [optional]
   * @param channel channel (if any)
   * @return current, convert to [A]
   */
  virtual double getCurrentProtect(unsigned channel = 0);

  /** \brief Get measured current
   * @param channel channel (if any)
   */
  virtual double measureCurrent(unsigned channel = 0) = 0;

  /** @} */

  /** \name Voltage Control and Measurement
   * @{
   */
  
  /** \brief Set voltage of PS
   * @param volt voltage [V]
   * @param channel channel (if any)
   */
  virtual void setVoltageLevel(double volt, unsigned channel = 0) = 0;

  /** \brief Get set voltage of PS.
   * @param channel channel (if any)
   * @return voltage, convert to [V]
   */
  virtual double getVoltageLevel(unsigned channel = 0) = 0;  

  /** \brief Set voltage protection
   * @param volt maximum current [A]
   * @param channel channel (if any)
   */
  virtual void setVoltageProtect(double volt, unsigned channel = 0);

  /** \brief Get voltage protection [optional]
   * @param channel channel (if any)
   * @return voltage, convert to [V]
   */
  virtual double getVoltageProtect(unsigned channel = 0);

  /** \brief Get measured voltage 
   * @param channel channel (if any)
   */
  virtual double measureVoltage(unsigned channel = 0) = 0;

  /** @} */

 protected:

  /** Store device configuration. */
  nlohmann::json m_config;

  /** Store device configuration name */
  std::string m_name;

};

#endif
