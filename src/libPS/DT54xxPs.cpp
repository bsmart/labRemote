#include "DT54xxPs.h"

#include <algorithm>
#include <thread>

#include "Logger.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(DT54xxPs)

DT54xxPs::DT54xxPs(const std::string& name) :
IPowerSupply(name)
{ }

void DT54xxPs::connect()
{
  if (m_config["communication"]["protocol"] == "usb")
    {
      connect_usb(m_config["communication"]["port"]);

      std::string result=command("MON","BDNAME");
      if (result.empty())
	{
	  logger(logERROR) << "Failed communication with the PS";
	  throw std::runtime_error("Failed communication with the PS");
	}
    }
  else
    {
      logger(logERROR) << "Unknown communication protocol for hardware " << m_name << ": " << m_config["communication"]["protocol"];
      throw std::runtime_error("Unknown communication protocol for hardware " + m_name + ": " + m_config["communication"]["protocol"].get<std::string>());
    }
}

void DT54xxPs::connect_usb(const std::string& dev)
{
  m_com.reset(new SerialCom(dev));
}

void DT54xxPs::reset()
{
  command("SET","OFF");
  command("SET","BDCLR");

  std::string result=command("MON","BDNAME");
  if(result.empty())
    throw std::runtime_error("No communication after reset.");
}

void DT54xxPs::checkCompatibilityList()
{

  // Check if the PS belong to the list of the support PS models
  std::string idn=command("MON","BDNAME");

  //Add here other version of support Keithley 24xx power supplies
  if(idn.find("DT5472") == std::string::npos)
    {
      logger(logERROR)<< "Unknown power supply: " << idn;
      throw std::runtime_error("Unknown power supply: " + idn);
    }
}

void DT54xxPs::turnOn(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET","ON");

  while(status()&Status::RampingUp)
    {
      double volt=measureVoltage();
      logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> ramping up: " << volt << "V";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void DT54xxPs::turnOff(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET","OFF");

  while(status()&Status::RampingDown)
    {
      double volt=measureVoltage();
      logger(logDEBUG) << __PRETTY_FUNCTION__ << " -> ramping down: " << volt << "V";
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}

void DT54xxPs::setCurrentLevel(double cur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET", "ISET", std::to_string(cur*1e6));
}

double DT54xxPs::getCurrentLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  return std::stod(command("MON","ISET"))/1e6;
}

void DT54xxPs::setCurrentProtect(double maxcur, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  setCurrentLevel(maxcur, channel);
}

double DT54xxPs::getCurrentProtect(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return getCurrentLevel(channel);
}

double DT54xxPs::measureCurrent(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(command("MON","IMON"))/1e6;
}

void DT54xxPs::setVoltageLevel(double volt, unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  command("SET", "VSET", std::to_string(volt));
}

double DT54xxPs::getVoltageLevel(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stod(command("MON","VSET"));
}

void DT54xxPs::setVoltageProtect(double maxvolt, unsigned channel)
{

  setVoltageLevel(maxvolt, channel);

}

double DT54xxPs::getVoltageProtect(unsigned channel)
{

  return getVoltageLevel(channel);
}

double DT54xxPs::measureVoltage(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");
  return std::stod(command("MON","VMON"));
}

uint16_t DT54xxPs::status(unsigned channel)
{
  if (channel != 1) throw std::runtime_error("Set the channel to 1 for single channel power-supply");

  return std::stoi(command("MON","STAT"))&0xFFFF;
}

void DT54xxPs::send(const std::string& cmd)
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\r\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
}

std::string DT54xxPs::receive(const std::string& cmd) 
{
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Sending: " << cmd;
  m_com->write(cmd+"\r\n");
  std::this_thread::sleep_for(std::chrono::milliseconds(m_wait));
  std::string buf;
  m_com->read(buf);
  logger(logDEBUG2) << __PRETTY_FUNCTION__ << " -> Received: " << buf;
  return buf;
}

std::string DT54xxPs::command(const std::string& cmd, const std::string& par, const std::string& value)
{
  // Build command
  std::string tosend="$CMD:"+cmd+",PAR:"+par;
  if(!value.empty())
    tosend+=",VAL:"+value;

  // Send command and receive response
  std::string resp=receive(tosend);

  // Parse response
  if(resp.empty())
    throw "DT54xx: No response :(";

  std::string retvalue;
  std::string cmdvalue;
  
  std::string token;
  std::stringstream ss(resp);
  while(std::getline(ss, token, ','))
    {
      size_t seppos=token.find(':');
      if(seppos==std::string::npos)
	continue; // Not a valid part
      if(token.substr(0,seppos)=="VAL")
	{ // This is the value part!
	  retvalue=token.substr(seppos+1);
	}
      else if(token.substr(0,seppos)=="#CMD")
	{ // This is the value part!
	  cmdvalue=token.substr(seppos+1);
	}
    }

  if(cmdvalue.empty())
    throw "DT54xx: No CMD in return statement :(";

  cmdvalue.erase(std::remove_if(cmdvalue.begin(), cmdvalue.end(), 
				[](char ch) { return ch=='\n' || ch=='\r'; }
				), cmdvalue.end());
  retvalue.erase(std::remove_if(retvalue.begin(), retvalue.end(), 
				[](char ch) { return ch=='\n' || ch=='\r'; }
				), retvalue.end());

  if(cmdvalue=="ERR")
    throw "DT54xx:: CMD shows an error :(";

  return retvalue;
}

