#include "AgilentE3631APs.h"

//Register power supply
#include "PowerSupplyRegistry.h"
REGISTER_POWERSUPPLY(AgilentE3631APs)

AgilentE3631APs::AgilentE3631APs(const std::string& name) :
AgilentPs(name, 3, {"E3631A"})
{ }

void AgilentE3631APs::turnOn(unsigned channel)
{ 
  AgilentPs::turnOn(channel);
}

void AgilentE3631APs::turnOff(unsigned channel)
{
  AgilentPs::turnOff(channel);
}
